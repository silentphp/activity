<?php
return array(
    'doctrine' => array(
        'driver' => array(
            'myblog_entity' => array(
                'class' =>'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                'paths' => array(__DIR__ . '/../src/MyBlog/Entity')
            ),
            'orm_default' => array(
                'drivers' => array(
                    'MyBlog\Entity' => 'myblog_entity',
                )
            )
        )
    ),
    
    
    'controllers' => array(
        'invokables' => array(
            'MyBlog\Controller\BlogPost' => 'MyBlog\Controller\BlogController',
            'MyBlog\Controller\Category' => 'MyBlog\Controller\CategoryController',
            'MyBlog\Controller\Resolve' => 'MyBlog\Controller\ResolveController',
        ),
    ),

    'router' => array(
        'routes' => array(
            'blog' => array(
                'type'    => 'segment',
                'options' => array(
                  //  'route'    => '/blog[/][:action][/:id]',
                    'route'    => '/blog[/][:action[/:id]]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id'     => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => 'MyBlog\Controller\BlogPost',
                        'action'     => 'index',
                    ),
                ),
            ),
            'category' => array(
                'type'    => 'segment',
                'options' => array(
                    'route'    => '/blog/category[/][:action][/:id]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id'     => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => 'MyBlog\Controller\Category',
                        'action'     => 'index',
                    ),
                ),
            ),
            'resolve' => array(
                'type'    => 'segment',
                'options' => array(
                    'route'    => '/blog/resolve[/][:action][/:id]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id'     => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => 'MyBlog\Controller\Resolve',
                        'action'     => 'index',
                    ),
                ),
            ),
        ),
    ),

    'view_manager' => array(
        'template_path_stack' => array(
            __DIR__ . '/../view',
        ),
    ),
);