<?php
/**
 * Created by PhpStorm.
 * User: silent
 * Date: 04.01.2016
 * Time: 13:14
 */

namespace MyBlog\Controller;


use MyBlog\Entity\Resolve;
use MyBlog\Form\ResolveForm;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use MyBlog\Entity;

class ResolveController extends AbstractActionController
{
    public function addAction()
    {
        return $this->editAction();
    }

    public function editAction()
    {
        $resolve = new Resolve;
        if ($this->params('id') > 0) {
            $blogpost = $this->getEntityManager()
                ->getRepository('MyBlog\Entity\BlogPost')
                ->find($this->params('id'));
        }
        $form = new ResolveForm($this->getEntityManager());
        $form->bind($resolve);
        $request = $this->getRequest();

        if ($request->isPost()) {
            //  $form->setInputFilter($category->getInputFilter());
            $form->setData($request->getPost());
            if ($form->isValid()) {
                $resolve->setCreated(time());
                $resolve->setBlogpost($blogpost);
                $em = $this->getEntityManager();
                $em->persist($resolve);
                $em->flush();
                //$this->flashMessenger()->addSuccessMessage('Category Saved');
                return $this->redirect()->toRoute('blog');
            }
        }
        return new ViewModel(array(
            'resolve' => $resolve,
            'form' => $form,
            'id' => $this->params('id')
        ));
    }

    private function getEntityManager(){
        if (null === $this->entityManager) {
            $this->entityManager = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        }
        return $this->entityManager;
    }

}