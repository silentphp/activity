<?php
/**
 * Created by PhpStorm.
 * User: silent
 * Date: 29.12.2015
 * Time: 2:33
 */

namespace MyBlog\Controller;

use Doctrine\ORM\EntityManager;
use Zend\Debug\Debug;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

use MyBlog\Form\CategoryForm;
use MyBlog\Entity\Category;
use MyBlog\Entity\BlogPost;

class CategoryController extends AbstractActionController
{
    public function indexAction()
    {
        /** @var EntityManager $objectManager */
        $objectManager = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        $categories = $objectManager
            ->getRepository('\MyBlog\Entity\Category')
            ->findAll();
        foreach($categories as $category){
            $posts = $objectManager
                ->getRepository('\MyBlog\Entity\BlogPost')
                ->findBy(array('category' => $category->getID()), array('created' => 'DESC'));
            $category->setPosts($posts);
        }

        $view = new ViewModel(array(
            'categories' => $categories,
        ));
        return $view;
    }

    public function addAction()
    {
        return $this->editAction();
    }

    public function editAction()
    {
        $category = new Category;
        if ($this->params('id') > 0) {
            $category = $this->getEntityManager()
                ->getRepository('MyBlog\Entity\Category')
                ->find($this->params('id'));
        }

        $form = new CategoryForm();
        $form->bind($category);
        $request = $this->getRequest();

        if ($request->isPost()) {
          //  $form->setInputFilter($category->getInputFilter());
            $form->setData($request->getPost());
            if ($form->isValid()) {
                $em = $this->getEntityManager();
                $em->persist($category);
                $em->flush();
                //$this->flashMessenger()->addSuccessMessage('Category Saved');
                return $this->redirect()->toRoute('category');
            }
        }
        return new ViewModel(array(
            'category' => $category,
            'form' => $form,
            'id' => $this->params('id')
        ));
    }

    public function deleteAction()
    {
        $category = $this->getEntityManager()
            ->getRepository('MyBlog\Entity\Category')
            ->find($this->params('id'));
        if ($category) {
            $em = $this->getEntityManager();
            $em->remove($category);
            $em->flush();
            $this->flashMessenger()->addSuccessMessage('Category Deleted');
        }
        return $this->redirect()->toRoute('category');
    }

    private function getEntityManager(){
        if (null === $this->entityManager) {
            $this->entityManager = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        }
        return $this->entityManager;
    }



}