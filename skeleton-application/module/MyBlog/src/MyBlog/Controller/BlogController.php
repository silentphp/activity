<?php
namespace MyBlog\Controller;
use MyBlog\Entity\BlogPost;
use MyBlog\Form\BlogPostForm;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use MyBlog\Entity;
use Zend\Debug\Debug;
class BlogController extends AbstractActionController
{
    /**
     * @return ViewModel
     */
    public function indexAction()
    {
        $objectManager = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        $id = $this->params('id');
        //Debug::dump($id, $label = null, $echo = true);
        if ($id == 0) {
            $posts = $objectManager
                ->getRepository('\MyBlog\Entity\BlogPost')
            ->findAll();
        }else{
            $category = $objectManager
                ->getRepository('\MyBlog\Entity\Category')
                ->find(array('id' => $id));
            $posts = $objectManager
                ->getRepository('\MyBlog\Entity\BlogPost')
                ->findBy(array('category' => $id), array('created' => 'DESC'));
        }
        $view = new ViewModel(array(
            'posts' => $posts,
            'category' => $category
        ));
        return $view;
    }
    public function viewAction()
    {
        $id = (int) $this->params()->fromRoute('id', 0);
        $objectManager = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        $post = $objectManager
            ->getRepository('\MyBlog\Entity\BlogPost')
            ->find(array('id' => $id));
        $resolves = $objectManager
            ->getRepository('\MyBlog\Entity\Resolve')
            ->findBy(array('blogpost' => $id), array('created' => 'DESC'));
        $view = new ViewModel(array(
            'post' => $post,
            'resolves' => $resolves
        ));
        return $view;
    }
    
    public function addAction()
    {
        return $this->editAction();
    }
    
    public function editAction()
    {
        $blogPost = new BlogPost();
        $objectManager = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        if ($this->params('id') > 0) {
            $blogPost = $objectManager
                ->getRepository('MyBlog\Entity\BlogPost')
                ->find($this->params('id'));
        }

        $form = new BlogPostForm($objectManager);
        $form->bind($blogPost);
        $request = $this->getRequest();
        if ($request->isPost()) {
            $form->setData($request->getPost());
            if ($form->isValid()) {
                $blogPost->setCreated(time());
                $objectManager->persist($blogPost);
                $objectManager->flush();
                // Redirect to list of blogposts
                return $this->redirect()->toRoute('blog');
            }
        }
        return array('form' => $form,'id' => $this->params('id'));
    }

    public function deleteAction()
    {
        $objectManager = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        $blogPost = $objectManager
            ->getRepository('MyBlog\Entity\BlogPost')
            ->find($this->params('id'));
        if ($blogPost) {
            $objectManager->remove($blogPost);
            $objectManager->flush();
            $this->flashMessenger()->addSuccessMessage('blogPost Deleted');
        }
        return $this->redirect()->toRoute('blog');
    }

}