<?php
/**
 * Created by PhpStorm.
 * User: silent
 * Date: 29.12.2015
 * Time: 2:22
 */

namespace MyBlog\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class Category
 * @package MyBlog\Entity
 *
 * @ORM\Entity
 * @ORM\Table(name="category")
 */

class Category
{
    /**
     * @var int
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    /**
     * @var string
     * @ORM\Column(type="string", length=255, nullable=false)
     */
    private $title;

    /**
     * @var object
     */
    private $posts;
    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }
    /**
     * Exchange array - used in ZF2 form
     *
     * @param array $data An array of data
     */
    public function exchangeArray($data)
    {
        $this->id = (isset($data['id']))? $data['id'] : null;
        $this->name = (isset($data['name']))? $data['name'] : null;
    }
    /**
     * Get an array copy of object
     *
     * @return array
     */
    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

    /**
     * @return posts
     */
    public function getPosts()
    {
        return $this->posts;
    }

    /**
     * @param  $posts
     */
    public function setPosts($posts)
    {
        $this->posts = $posts;
    }


}