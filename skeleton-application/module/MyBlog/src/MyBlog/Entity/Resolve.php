<?php
/**
 * Created by PhpStorm.
 * User: silent
 * Date: 03.01.2016
 * Time: 16:36
 */

namespace MyBlog\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class Resolve
 * @package MyBlog\Entity
 *
 * @ORM\Entity
 * @ORM\Table(name="resolve")
 */
class Resolve
{
    /**
     * @var int
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var text
     * @ORM\Column(type="text")
     */
    private $text;

    /**
     * @var blogpost
     * @ORM\ManyToOne(targetEntity="BlogPost", cascade={"persist"})
     * @ORM\JoinColumn(name="blogpost_id", referencedColumnName="id")
     */
    private $blogpost;

    /**
     * @var int
     * @ORM\Column(type="integer")
     */
    private $created;

    /**
     * @return text
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * @param text $text
     */
    public function setText($text)
    {
        $this->text = $text;
    }

    /**
     * @return blogpost
     */
    public function getBlogpost()
    {
        return $this->blogpost;
    }

    /**
     * @param blogpost $blogpost
     */
    public function setBlogpost($blogpost)
    {
        $this->blogpost = $blogpost;
    }

    /**
     * @return int
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param int $created
     */
    public function setCreated($created)
    {
        $this->created = $created;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }
    /**
     * Get an array copy of object
     *
     * @return array
     */
    public function getArrayCopy()
    {
        return get_object_vars($this);
    }
    public function exchangeArray($data)
    {
        foreach ($data as $key => $val) {
            if (property_exists($this, $key)) {
                $this->$key = ($val !== null) ? $val : null;
            }
        }
    }

}