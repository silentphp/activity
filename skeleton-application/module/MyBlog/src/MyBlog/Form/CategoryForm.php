<?php
/**
 * Created by PhpStorm.
 * User: silent
 * Date: 31.12.2015
 * Time: 13:00
 */

namespace MyBlog\Form;
use Zend\Form\Form;
use Zend\Stdlib\Hydrator\ClassMethods;

class CategoryForm extends Form
{
    public function __construct(){
        parent::__construct('category');
        //$this->setAttribute('method', 'post');
     //   var_dump(new ClassMethods());
        $this->setAttribute('method', 'post')
            ->setHydrator(new ClassMethods());
        $this->add(array(
            'name' => 'id',
            'type' => 'Hidden',
        ));
        $this->add(array(
            'name' => 'title',
            'type'  => 'text',
            'options' => array('label' => 'Name',),
        ));
        $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type'  => 'submit',
                'value' => 'Save',
                'id' => 'submitbutton',
            ),
        ));
    }

}