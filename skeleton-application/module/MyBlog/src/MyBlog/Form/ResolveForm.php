<?php
/**
 * Created by PhpStorm.
 * User: silent
 * Date: 04.01.2016
 * Time: 13:17
 */

namespace MyBlog\Form;

use Doctrine\ORM\EntityManager;
use DoctrineModule\Stdlib\Hydrator\DoctrineObject;
use Zend\Form\Form;
use Zend\Stdlib\Hydrator\ClassMethods;

class ResolveForm extends Form
{
    public function __construct(EntityManager $em){
        parent::__construct('resolve');
        $this->setAttribute('method', 'post')->
        setHydrator(new DoctrineObject($em, 'MyBlog\Entity\Post'));
        $this->add(array(
            'name' => 'id',
            'type' => 'Hidden',
        ));
        $this->add(array(
            'name' => 'text',
            'type' => 'Textarea',
            'options' => array(
                'label' => 'Решение',
            ),
        ));
        $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type'  => 'submit',
                'value' => 'Save',
                'id' => 'submitbutton',
            ),
        ));
    }

}