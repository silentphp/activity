<?php
namespace MyBlog\Form;
use DoctrineModule\Stdlib\Hydrator\DoctrineObject;
use Zend\Form\Form;
use Doctrine\ORM\EntityManager;
use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;

class BlogPostForm extends Form
{
    public function __construct(EntityManager $em)
    {
        parent::__construct('blogpost');
        //$this->setAttribute('method', 'post');
        $this->setAttribute('method', 'post')->
        setHydrator(new DoctrineObject($em, 'MyBlog\Entity\Post'));
        /*$this->setAttribute('method', 'post')
            ->setHydrator(new ClassMethods());*/
        $this->setInputFilter(new \MyBlog\Form\BlogPostInputFilter());
        /* $this->add(array(
            'name' => 'security',
            'type' => 'Zend\Form\Element\Csrf',
        )); */
        $this->add(array(
            'name' => 'id',
            'type' => 'Hidden',
        ));
        $this->add(array(
            'name' => 'created',
            'type' => 'Hidden',
            /*'attributes' => array(
                'value' => '1',
            )*/
        ));
        /*$this->add(array(
            'name' => 'userId',
            'type' => 'Hidden',
        ));*/
        $this->add(array(
            'name' => 'title',
            'type' => 'Text',
            'options' => array(
                'min' => 3,
                'max' => 25,
                'label' => 'Title',
            ),
        ));
        $this->add(array(
            'name' => 'text',
            'type' => 'Textarea',
            'options' => array(
                'label' => 'Text',
            ),
        ));

        $this->add(array(
            'name' => 'category',
            'type' => 'DoctrineModule\Form\Element\ObjectSelect',
            'options' => array(
                'label' => 'Category',
                'object_manager' => $em,
                'target_class' => 'MyBlog\Entity\Category',
                'property' => 'title'
            ),
        ));
        
        $this->add(array(
            'name' => 'submit',
            'type' => 'Submit',
            'attributes' => array(
                'value' => 'Save',
                'id' => 'submitbutton',
            ),
        ));
    }
}